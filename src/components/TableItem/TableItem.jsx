import React from 'react';
import './TableItem.css';

function TableItem(props) {
    // state = {  }
    return (
        <tr>
            <td>{props.data.name}</td>
            <td>{props.data.dept}</td>
            <td>{props.data.backlogs}</td>
            <td>
                <button onClick={() => props.tableActions.onRemoveBlCount(props.data.id)}>Remove</button>
                <button onClick={() => props.tableActions.onRemovePerson(props.data.id)} disabled={props.data.backlogs !== 0}>Delete</button>
            </td>
        </tr>
    );
}

export default TableItem;