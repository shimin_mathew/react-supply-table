import React from 'react';
// import TableItem from '../TableItem/TableItem'
import './Table.css';

function Table(props) {
    return (
        <table>
            <thead>
                <tr>
                    {props.head.map((head, i) => <th key={i}>{head}</th>)}
                </tr>
            </thead>
            <tbody>
                {props.children}
            </tbody>
        </table>
    );
}

export default Table;