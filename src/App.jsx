import React from 'react';
// import logo from './logo.svg';
import Table from './components/Table/Table'
import TableItem from './components/TableItem/TableItem';
// import './App.css';

class App extends React.Component {
  state = {
    data: [
      { name: 'steve', dept: 'cse', backlogs: 1 },
      { name: 'shimin', dept: 'cse', backlogs: 1 },
      { name: 'alan', dept: 'cse', backlogs: 1 },
      { name: 'p', dept: 'me', backlogs: 5 }
    ]
  }

  constructor(props) {
    super(props);
    this.state.data.map((data, i) => data.id = i);
  }

  removeBl = (id) => {
    this.setState({
      data: this.state.data.map(d => {
        if (d.id === id && d.backlogs !== 0) {
          --d.backlogs;
        }
        return d;
      })
    });
  }

  removePerson = (id) => {
    this.setState({
      data: this.state.data.filter(person => {
        if (person.id === id && person.backlogs === 0) {
          return false;
        }
        return true;
      })
    });
  }

  render() {
    if (this.state.data.length === 0) {
      return <h1 className="msg-empty">Nobody Remains!</h1>
    } else {
      let th = Object.keys(this.state.data[0]).filter(ele => ele !== 'id');
      return (
        <Table head={th}>
          {this.state.data.map((ppl, i) => <TableItem key={i} data={ppl} tableActions={{
            onRemoveBlCount: this.removeBl,
            onRemovePerson: this.removePerson
          }} />)}
        </Table>
      );
    }
  }
}

export default App;
